const express = require('express')
const app = express()
const port = 3003
const pjson = require('./package.json')

app.get('/', (req, res) => {
  res.send('' + pjson.version)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})