const express = require('express')
const app = express()
const fetch = require('node-fetch');
const port = 8080
const pjson = require('./package.json')
var EventEmitter = require("events").EventEmitter;

var foo = new EventEmitter();
var bar = new EventEmitter();

async function httpget(url, param) {
    let response = await fetch(url);
    let data = await response.text();
    param.data = data
}

app.get('/', async (req, res) => {
    await httpget('http://localhost:3001/', foo)
    await httpget('http://localhost:3003/', bar)

    res.send({
        "bar version: ": bar.data,
        "foo version: ": foo.data,
        "message: ": "pong",
        "version: ": pjson.version
    })
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})